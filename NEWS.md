## ncodeR 0.2.1.1
  * Bugfix: Error in resolve with add/remove expressions

## ncodeR 0.2.1.0
  * Implemented the `unseen` parameter in `handcode()`.  Adds an additional 
    two excerpts to test set that contain the highest occurrence of words 
    unseen by the coder

## ncodeR 0.2.0.1 (2019-11-19)
  * Bugfix: Missing precision and recall calculation with rho/Kappa
  * Bugfix: Fixed output from autocode()

## ncodeR 0.2.0.0 (2019-10-09)
  * Restructed statistical history on codes
  * test() defaults kappa_threshold to 0.9
  * Codes now contain a holdout set and a set of touchable excerpts.
  * Simplified retrieving of handsets for coding, along with randomizing the order
  * Bugfix: handcode() bug fixed that didn't show full excerpt when there was a newline.
  * Bugfix: Fixed bug when retreiving differences in code sets using differences()

## ncodeR 0.1.3.0 (2019-04-09)
  * handcode() will now invalidate TestSet if it's already been tested
  * test() now keeps a running history of all tests on the Code
  * Bugfix: summary() will again properly show the most statistics results
  * Bugfix: resolve() properly shows training kappas
  * Bugfix: handcode() will now account for already coded excerpts when pulling more excerpts

## ncodeR 0.1.2 (2018-08-29)
  * autocode() now has a first parameter x that accepts either a Code or a CodeSet object
  * Bugfix: Using a CodeSet to autocode now works again
  * Bugfix: as.data.frame.CodeSet now works as intended

## ncodeR 0.1.1 (2018-08-17)
  * Excerpts can now by attached directly to a code
  * Coding performance improved, allowing use of larger excerpt files
  * While resolving differences, only the TrainingSet will be recoded

## ncodeR 0.1.0 (2018-06-15)
  * Initial Release