testthat::test_that("standard conversion", {
  # Individual Pieces
  data(RS.data)
  rs = RS.data
  newcode = create.code(name = "Data", expressions = c("number","data"), excerpts = rs$text)
  
  df <- as.data.frame(newcode)
  testthat::expect_is(df, "data.frame")
  testthat::expect_equal(nrow(df), nrow(rs))
})

testthat::test_that("reduced conversion", {
  # Individual Pieces
  data(RS.data)
  rs = RS.data
  newcode = create.code(name = "Data", expressions = c("number","data"), excerpts = rs$text)
  
  df <- as.data.frame(newcode, len = 100)
  testthat::expect_is(df, "data.frame")
  testthat::expect_equal(nrow(df), 100)
})

testthat::test_that("standard conversion of CodeSet", {
  # Individual Pieces
  data(RS.data)
  rs = RS.data
  newcode = create.code(name = "Data", expressions = c("number","data"), excerpts = rs$text)
  newcode2 = create.code(name = "Team", expressions = c("group","team"), excerpts = rs$text)
  code.set = code.set("Test RS CodeSet", "CodeSet made for testing", codes = c(newcode, newcode2))
  
  df <- as.data.frame(code.set)
  testthat::expect_is(df, "data.frame")
  testthat::expect_equal(nrow(df), nrow(rs))
})

testthat::test_that("reduced conversion of CodeSet", {
  # Individual Pieces
  data(RS.data)
  rs = RS.data
  newcode = create.code(name = "Data", expressions = c("number","data"), excerpts = rs$text)
  newcode2 = create.code(name = "Team", expressions = c("group","team"), excerpts = rs$text)
  code.set = code.set("Test RS CodeSet", "CodeSet made for testing", codes = c(newcode, newcode2))
  
  df <- as.data.frame(code.set, len = 100)
  testthat::expect_is(df, "data.frame")
  testthat::expect_equal(nrow(df), 100)
})

testthat::test_that("force empty use CodeSet", {
  # Individual Pieces
  data(RS.data)
  rs = RS.data
  newcode = create.code(name = "Data", expressions = c("number","data"), excerpts = rs$text)
  newcode2 = create.code(name = "Team", expressions = c("group","team"), excerpts = rs$text)
  code.set = code.set("Test RS CodeSet", "CodeSet made for testing", codes = c(newcode, newcode2))
  
  df <- as.data.frame(code.set, len = 0)
  testthat::expect_is(df, "data.frame")
  testthat::expect_equal(nrow(df), 0)
})
