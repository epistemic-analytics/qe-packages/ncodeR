# ncodeR <img src="man/figures/logo.png" align="right" alt="" width="120" />


## What is nCoder 

A set of techniques that can be used to develop, validate, and implement automated classifiers. A powerful tool for transforming raw data into meaningful information, 'ncodeR' (Shaffer, D. W. (2017) Quantitative Ethnography. ISBN: 0578191687) is designed specifically for working with big data: large document collections, logfiles, and other text data.

## Installation

### Install release version from CRAN
[![cran status](https://www.r-pkg.org/badges/version-ago/ncodeR)](https://cran.r-project.org/package=ncodeR) 
[![cran downloads](https://cranlogs.r-pkg.org/badges/grand-total/ncodeR)](https://cranlogs.r-pkg.org/badges/grand-total/ncodeR) 

```
install.packages("ncodeR")
```

### Install development version
[![pipeline status](https://gitlab.com/epistemic-analytics/qe-packages/ncodeR/badges/master/pipeline.svg)](https://gitlab.com/epistemic-analytics/qe-packages/ncodeR/-/commits/master)
[![coverage report](https://gitlab.com/epistemic-analytics/qe-packages/ncodeR/badges/master/coverage.svg)](https://gitlab.com/epistemic-analytics/qe-packages/ncodeR/-/commits/master)

```
install.packages("ncodeR", repos = "https://cran.qe-libs.org")
```

